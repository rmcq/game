(set-env! :repositories #(conj % ["clojars" {:url "https://clojars.org/repo/"}]))

(set-env! :dependencies
          `[[org.clojure/clojure ~(clojure-version) :scope "provided"]
            [org.clojure/tools.nrepl "0.2.12"]
            [org.clojure/core.async "0.3.443"]
            [cider/cider-nrepl "0.15.0-SNAPSHOT"]
            [org.clojure/clojurescript "1.9.671"]
            [adzerk/boot-cljs "2.0.0"]
            [binaryage/devtools "0.9.2"]
            [figwheel-sidecar/figwheel-sidecar "0.5.10"]
            [com.cemerick/piggieback "0.2.1"]
            [ring/ring "1.6.1"]
            [http-kit/http-kit "2.2.0"]
            [compojure/compojure "1.6.0"]
            [aleph/aleph "0.4.3"]
            [gloss/gloss "0.2.6"]
            [criterium/criterium "0.4.4"]
            [org.dyn4j/dyn4j "3.2.4"]]

          :source-paths #{"src"}
          :resource-paths  #{"static"})

(require '[adzerk.boot-cljs :refer [cljs]])

(task-options!
 cljs {:optimizations :advanced})

(deftask build
  []
  (comp
   (aot :all true)
   (uber)
   (jar :file "out.jar" :main 'sys)
   (sift :include #{#"out.jar"})
   (target)))

