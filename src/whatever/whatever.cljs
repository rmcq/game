(ns whatever.whatever
  (:require-macros [cljs.core.async.macros :refer [go go-loop]])
  (:require
   [clojure.string :as string]
   [cljs.core.async :refer [chan <! put! >! timeout to-chan sliding-buffer]]
   [whatever.common :as common]))

(enable-console-print!)

(def *canvas*  (.getElementById js/document (:id common/canvas-props)))
(def *context* (.getContext *canvas* "2d"))
(def *fps-display* (.getElementById js/document "fps"))

(defn clear
  [color]
  (let [c       *context*
        prev-fs (.-fillStyle c)]
    (set! (.-fillStyle c) color)
    (.fillRect c 0 0 (.-width *canvas*) (.-height *canvas*))
    (set! (.-fillStyle c) prev-fs)))

(def tau (* 2 (.-PI js/Math)))

(defn draw-circle
  [c x y r]
  (.beginPath c)
  (.arc c x y r 0 tau)
  (.fill c))

(defonce last-timestamp (atom 0))

(defonce running (atom false))

#_(defn animate'
  [e f]
  ((fn anim-frame [timestamp]
     (let [delay-ms (- timestamp @last-timestamp)]
       (set! (.-textContent *fps-display*) (/ 1000 delay-ms)))
     (reset! last-timestamp timestamp)
     (let [{:keys [x y]} @e]
       (draw-circle *context* x y 6)
       (swap! e f))
     (when @running
       (.requestAnimationFrame js/window anim-frame)))))

(clear "#000000")

(defonce prev-state (atom nil))
(defonce prev-state-timestamp (atom 0))
(defonce target-state (atom nil))
(defonce target-state-timestamp (atom 0))
(defonce last-render-timestamp (atom (.now js/performance)))

(def colors
  (into []
        (for [i (range 255)]
          (str "rgb(" i "," i "," i ")")
          ;(format "rgb(%d, %d, %d)" i i i)
          )))

(defn websocket-message-listener
  [ev]
  (let [dataview      (js/DataView. (.-data ev))
        sizeof-float  4
        timestamp     (.now js/performance)]
    (clear "#eee")
    (loop [byte-pos 0]
      (when (< byte-pos (.-byteLength dataview))
        (set! (.-fillStyle *context*) (nth colors (/ byte-pos 12)))
        (draw-circle *context*
                     (.getFloat32 dataview byte-pos)
                     (.getFloat32 dataview (+ 4 byte-pos))
                     (.getFloat32 dataview (+ 8 byte-pos)))
        (recur (+ 12 byte-pos))))
    (set! (.-textContent *fps-display*) (- timestamp @last-timestamp))
    (reset! last-timestamp timestamp)))

(defonce connection (atom nil))

(defn connect []
 (reset! connection
         (let [origin (.. js/window -location -origin)
               sock  (js/WebSocket. (str (string/replace origin "http://" "ws://") "/ws"))]
           (set! (.-binaryType sock) "arraybuffer")
           #_(.addEventListener sock "open" (fn [ev]
                                              (.send sock "test!")))
           (.addEventListener sock "message" websocket-message-listener)
           sock)))

(if @connection (.close @connection))
(connect)

(clear "#eee")
(set! (.-fillStyle *context*) "#000000")
(defn on-js-reload []
  (reset! running false)
  (.setTimeout js/window #(reset! running true)))
