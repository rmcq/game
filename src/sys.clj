(ns sys
  (:require
   [clojure.main :as main]
   [clojure.tools.nrepl.server :as nrepl-server]
   [cider.nrepl :refer (cider-nrepl-handler)]
   [figwheel-sidecar.repl-api :as fw]
   [compojure.core :refer [defroutes GET ANY]]
   [ring.util.response :as resp]
   [aleph.http :as http]
   [manifold.stream :as s]
   ;;[criterium.core :as c]
   [hiccup.page :as hp]
   [whatever.common :as common])
  (:import
   [java.nio ByteBuffer]
   [io.netty.buffer ByteBuf Unpooled]
   [org.dyn4j.collision.manifold Manifold]
   [org.dyn4j.collision.narrowphase Penetration]
   [org.dyn4j.dynamics Body BodyFixture World CollisionAdapter]
   [org.dyn4j.geometry Capsule Convex Geometry Circle Rectangle MassType Vector2])
  (:gen-class))

(def running (atom true))

(defn start-figwheel []
  (fw/start-figwheel!
   {:build-ids ["dev"]
    :figwheel-options { :reload-clj-files false }
    :all-builds
    [{:id "dev"
      :figwheel {:heads-up-display true
                 :autoload true
                 :debug false                 
                 :on-jsload "whatever.whatever/on-js-reload"}
      :source-paths ["src"]
      :compiler {:main "whatever.whatever"
                 :asset-path "resources"
                 :output-to "resources/figwheel.js"
                 :output-dir "resources"
                 :verbose true}}]}))


(defn get-resource
  [rsrc content-type]
  (GET (str "/" rsrc) []
       (some-> (resp/resource-response rsrc)
               (resp/content-type content-type))))

(def snapshot (atom nil))

(defn get-send-interval [] 22)

(defn dedupe-identical
  "Just like (dedupe) except using identical? instead of ="
  [rf]
  (let [pv (volatile! ::none)]
    (fn
      ([] (rf))
      ([result] (rf result))
      ([result input]
       (let [prior @pv]
         (vreset! pv input)
         (if (identical? prior input)
           result
           (rf result input)))))))

(defroutes routes
  (get-resource "main.js"    "application/javascript")
  (GET "/app" []
       (hp/html5
        [:canvas common/canvas-props]
        [:div [:span {:id "fps"}]]
        (hp/include-js "resources/figwheel.js")))
  (GET "/ws" req
       (let [ws-stream @(http/websocket-connection req)]
         (s/connect
          (s/map #(.retain (Unpooled/wrappedBuffer (.array %) 0 (.position %)))
                 (s/transform dedupe-identical
                              (s/periodically (get-send-interval) #(deref snapshot))))
          ws-stream)))
  (GET "*" req
       (resp/file-response (.substring (:uri req) 1))))


(defn createBall [width height]
  (doto (Body.)
    (.addFixture (Circle. (+ 11 (* 33 (rand)))))
    (.setMass MassType/NORMAL)
    (.translate (* width (rand)) (* height (rand)))
    (.setLinearVelocity (Vector2. (* (rand) 1534) (* 1252 (rand))))
    (.setAutoSleepingEnabled true)
    (.setUserData :real)))


(defn run-test []
  (let [world                   (World.)
        cir-shape               (Circle. 16)
        nbodies                 128
        data-size               (* nbodies 12)
        history-size            128
        _                       (println "total size = " data-size " * " history-size " bytes = " (* data-size history-size))
        buffers                 (cycle (repeatedly history-size #(ByteBuffer/allocate data-size)))
        {:keys [width, height]} common/canvas-props]
    (doto world
      (.addBody (doto (Body.) ;; left wall (at x=0 with full height)
                  (.addFixture (Geometry/createSegment (Vector2. 0.0 0.0) (Vector2. 0.0 height)))
                  (.setMass MassType/INFINITE)))
      (.addBody (doto (Body.) ;; right wall (at x=width with full height)
                  (.addFixture (Geometry/createSegment (Vector2. width 0.0) (Vector2. width height)))
                  (.setMass MassType/INFINITE)))
      (.addBody (doto (Body.) ;; top wall (at y=0 with full width)
                  (.addFixture (Geometry/createSegment (Vector2. 0.0 0.0) (Vector2. width 0.0)))
                  (.setMass MassType/INFINITE)))
      (.addBody (doto (Body.) ;; bottom wall (at y=height with full width)
                  (.addFixture (Geometry/createSegment (Vector2. 0.0 height) (Vector2. width height)))
                  (.setMass MassType/INFINITE)))
      (.addBody (doto (Body.)
                  (.addFixture (Geometry/createEquilateralTriangle 222))
                  (.translate (/ width 2.0) 990)
                  ;;(.rotate 1)
                  (.setUserData :spin)
                  (.rotateAboutCenter 1)
                  (.setMass MassType/INFINITE))))
    #_(.addListener
     world
     )

    (dotimes [i nbodies]
      (.addBody world (createBall width 222)))

    ;;(.setGravity world World/ZERO_GRAVITY)
    (.setGravity world (Vector2. 0.0 388.0))
    (-> world
        (.getSettings)
        (.setStepFrequency (/ 1.0 333)))

    (.addListener world (collider. world))

    (def world-thread
      (Thread. 
       (fn []
         (time
          (loop [last-time (System/nanoTime)
                 stepnum   0
                 bufs      buffers]
            (let [time      (System/nanoTime)
                  diff      (- time last-time)
                  buf       (first bufs)
                  to-remove (volatile! (transient []))]
              (.updatev world (/ diff 1.0e9))
              (.position buf 0)
              (doseq [^Body b (.getBodies world)]
                (when (identical? :spin (.getUserData b))
                  (.rotateAboutCenter b 0.01))
                (when (identical? :real (.getUserData b))
                  (let [tx (.getTransform b)]
                    (.putFloat buf (float (.getTranslationX tx)))
                    (.putFloat buf (float (.getTranslationY tx)))
                    (.putFloat buf (float (.getRotationDiscRadius b))))
                  (when (< (.normalize (.copy (.getLinearVelocity b))) 0.1)
                    (vreset! to-remove (conj! @to-remove b)))))
              (reset! snapshot buf)
              (doseq [^Body b (persistent! @to-remove)]
                (.removeBody world b)
                (.addBody world (doto (createBall width 33))))
              (if @running
                (recur time (inc stepnum) (next bufs))
                (println "steps:" stepnum))))))))
    (println "================================================================================")
    (reset! running true)
    (.start world-thread)))

(comment
  (reset! running false)

  (do ;;rerun
    (reset! running false)
    (Thread/sleep 111)
    (run-test))

  (into []
        (map #(vector (float (.x %)) (float (.y %))))
        (iterator-seq (.getVertexIterator (Geometry/createPolygonalCircle 6 100))))

  (identity @snapshot)
  (start-figwheel)
  (fw/stop-figwheel!)
  (run-test)
  (http/start-server routes {:port 8888}))

(set! *unchecked-math* true)
(defn -main
  []
  (nrepl-server/start-server :port 7888 :handler cider-nrepl-handler)
  (println "started nrepl")
  ;;(run-test)
  (start-figwheel)
  (run-test)
  (http/start-server routes {:port 3333}) 
  ;;(println "started server")
  ;;(run-test)
)
