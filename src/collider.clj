(ns collider
  (:import
   [org.dyn4j.collision.manifold Manifold]
   [org.dyn4j.collision.narrowphase Penetration]
   [org.dyn4j.dynamics World Body BodyFixture CollisionAdapter])
  (:gen-class :extends org.dyn4j.dynamics.CollisionAdapter
              :state world
              :init init
              :constructors {[org.dyn4j.dynamics.World] []}
              :prefix m-))

(defn collide-broadphase
  [world ba fa bb fb]
  true
  #_(if (and (.getUserData ba) (.getUserData bb))
    (do
      (.removeBody world ba)
      false)
    true))

(defn collide-narrowphase
  [world ba fa bb fb p]
  true)

(defn collide-manifold
  [world ba fa bb fb m]
  true)

(defn collide-contact
  [world cc]
  true)

;;https://stackoverflow.com/questions/32773861/clojure-gen-class-for-overloaded-and-overridden-methods
;;https://kotka.de/blog/2010/02/gen-class_how_it_works_and_how_to_use_it.html
(defn m-init
  [world]
  [[] world])

(defn m-collision-CollisionAdapter
  [this cc]
  (collide-contact (.world this) cc))

(defn m-collision-Body-BodyFixture-Body-BodyFixture
  [this ba fa bb fb]
  (collide-broadphase (.world this) ba fa bb fb))

(defn m-collision-Body-BodyFixture-Body-BodyFixture-Penetration
  [this ba fa bb fb p]
  (collide-narrowphase (.world this) ba fa bb fb p))

(defn m-collision-Body-BodyFixture-Body-BodyFixture-Manifold
  [this ba fa bb fb m]
  (collide-manifold (.world this) ba fa bb fb m))

